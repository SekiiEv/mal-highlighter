# MyAnimeList Highlighter

Tampermonkey script for MyAnimeList.net written in Javascript. The script can be download and used [here](https://greasyfork.org/en/scripts/376552-myanimelist-artist-entry-colored-highlighting)

MyAnimeList is an online database of anime (japanese animation) and manga (japanese comic). Any user can access the database to make a list of works they have watched/read.

The objective of this extension is to highlight those shows when accessing an artist entry on the database. The color is chosen based on how the user has marked the show (watching, watched, on-hold, dropped or planned to watch).

![](git-img/Example.png)